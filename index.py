import flights
from flights import Flights

some_data=Flights()

print(some_data.__doc__)
task_number=input("Введите номер выбранной задачи: ")

if task_number=="0":
    pet_name=input("введите имя питомца: ")
    task=Flights()
    dis=task.test(pet_name)
    print(dis)

if task_number=="1":
    '''task №1 На вход дается номер рейса. Надо посчитать длину пути.
        То есть расстояние между первой и последней точкой'''
    flight_number=input("введите номер рейса: ")
    task=Flights()
    try:
        distance=task.distance(flight_number=flight_number)
        print("Дальность полета составит: ",distance, " км")
    except KeyError:
        print("Введенного вами рейса не существует, повторите попытку позже ;-)")

elif task_number=="2":
    '''task №2 На вход дается номер рейса.
        Надо посчитать во сколько сядет самолет'''
    flight_number=input("введите номер рейса: ")
    task=Flights()
    try:
        arrival_time=task.arrival_time(flight_number=flight_number)
        print("Время прибытия: ",arrival_time)
    except KeyError:
        print("Введенного вами рейса не существует, повторите попытку позже ;-)")

elif task_number=="3":
    '''task №3 На вход дается номер рейса и точка(координаты точки).
        Надо определить во сколько самолет будет на этой точке'''
    flight_number = input("Введите номер рейса, у которого желаете узнать время: ")
    new_coord = input("Введите координаты полета: ")
    task=Flights()
    try:
        distance=task.dist_to_point(flight_number=flight_number, coordinates=new_coord)
        print("Время прибытия до заданной точки составляет: ",distance)
    except UnboundLocalError:
        print("""Введенные координаты наш самолетик не пролетает,
        попробуйте угадать еще раз ;-)""")
    except KeyError:
        print("Введенного вами рейса не существует, повторите попытку позже ;-)")
    except ValueError:
        print("""Введенный вами формат не соответствует международному формату
        координат: xx.yy повторите попытку позже ;-)""")

elif task_number=="4":
    '''task №4 Дается время. Нужно определить сколько самолетов
        находятся в воздухе в заданное время'''
    input_time=input("Введите время: ")
    task=Flights()
    try:
        number_of_flights=task.number_of_flights(input_time=input_time)
        print("""Всего в воздушном пространстве земного шарика
        находится """,number_of_flights, "самолетиков")
    except UnboundLocalError:
        print("""К сожалению ни одного самолетика в указанное время нет в воздушном
        пространстве земного шарика попробуйте ввести другое время позже ;-)""")
    except ValueError:
        print("""Введенный вами формат не соответствует формату DateTime:
        yyyy.mm.dd hh.mm.ss повторите попытку позже ;-)""")

elif task_number=="5":
    '''task №5 На вход дается номер рейса и точка(координаты точки).
        Надо определить какое расстояние от точки начала до заданной'''
    flight_number=input("Введите номер рейса: ")
    new_coordinates=input("Введите новые координаты для данного рейса: ")
    task=Flights()
    try:
        distance=task.dist_to_new_point(flight_number=flight_number, new_coordinates=new_coordinates)
        print("Дальность полета составит: ", distance, " км")
    except KeyError:
        print("Введенного вами рейса не существует, повторите попытку позже ;-)")
    except ValueError:
        print("""Введенный вами формат не соответствует международному формату
        координат: xx.yy повторите попытку позже ;-)""")
        

else: print("К сожалению задачи с таким номером нет, попробуйте выбрать другую задачу")



