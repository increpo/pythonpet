class Flights:
    '''
Задача №1: На вход дается номер рейса. Надо посчитать длину пути.
           То есть расстояние между первой и последней точкой
Задача №2: На вход дается номер рейса. Надо посчитать во сколько сядет самолет
Задача №3: На вход дается номер рейса и точка(координаты точки).
           Надо определить во сколько самолет будет на этой точке
Задача №4: Дается время. Нужно определить сколько самолетов находятся в воздухе
           заданное время
Задача №5: На вход дается номер рейса и точка(координаты точки).
           Надо определить какой расстояние от точки начала до заданной
       '''
    def __init__(self):
        self.args = []
       
        #считываем файл с рейсами
        with open("routes.txt", "r") as file:
            file_data=file.read()

        import pylibmc
        mc = pylibmc.Client(["127.0.0.1"], binary=True,
                    behaviors={"tcp_nodelay": True,
                                "ketama": True})
        import json
        jt = json.loads(file_data)

        mc.set_multi(jt)
        self.routes=mc.get_multi(["routes"])["routes"]


    def test(self, arg):
        '''на это не обращаем внимания, это для тестов'''
        self.args.append(arg)
        distance=self.distance("FV4673")

        return distance
    
    def datetime_converter(self, dattime):
        '''Конвертирует сроку в формат datetime'''
        import datetime
        self.args.append(dattime)
        year=int(dattime[:4])
        month=int(dattime[5:7])
        day=int(dattime[8:10])
        hour=int(dattime[11:13])
        minute=int(dattime[14:16])
        second=int(dattime[17:19])
        datetime_convert=datetime.datetime(year,month,day,hour,minute,second)

        return datetime_convert
    
    def dist_time_converter(self, dist_time):
        '''Конвертирует сроку в формат timedelta(hours, minutes)'''
        import datetime
        from datetime import timedelta
        self.args.append(dist_time)
        hour=int(str(dist_time)[:-3])
        minute=round(int(str(dist_time)[-2::1])*60/100)                
        dist_time_convert=timedelta(hours=hour, minutes=minute)

        return dist_time_convert
    
    def dist(self, coordinates, d):
        '''Рассчитывает расстояние между точками по координатам расстояние'''
        import math
        self.args.append(coordinates)
        self.args.append(d)
        x1=coordinates[d][0]
        y1=coordinates[d][1]
        x2=coordinates[d+1][0]
        y2=coordinates[d+1][1]
        R=6372.79 #радиус Земли
        dist=R*math.acos(math.sin(x1)*math.sin(x2)+math.cos(x1)*math.cos(x2)*math.cos(y1-y2))
        dist_round=round(dist, 2)

        return dist_round             
    
    def distance(self, flight_number):
        '''Задача №1: На вход дается номер рейса. Надо посчитать длину пути.
        То есть расстояние между первой и последней точкой'''
        d=0
        distance_summ=0
        routes = self.routes
        self.args.append(flight_number)

        for i in routes[flight_number]['tr']:
            if len(routes[flight_number]['tr'])>d+1:       
                dist=self.dist(coordinates=routes[flight_number]['tr'],d=d) 
                d+=1
                distance_summ+=dist
            else: break
        
        return distance_summ

    def arrival_time(self, flight_number):
        '''Задача №2: На вход дается номер рейса. Надо посчитать во сколько сядет самолет'''
        import math
        x=0
        distance_summ=0
        routes = self.routes
        self.args.append(flight_number)
        distance_summ=self.distance(flight_number)

        #считаем общее время полета
        flight_time=round(distance_summ*100/int(routes[flight_number]['speed']), 2)
        #конвертируем время взлета в формат datetime
        start_time_convert=self.datetime_converter(routes[flight_number]['start'])
        dist_time_convert=self.dist_time_converter(flight_time)
        arrival_time=start_time_convert + dist_time_convert

        return arrival_time

    def dist_to_point(self, flight_number, coordinates):
        '''Задача №3: На вход дается номер рейса и точка(координаты точки).
        Надо определить во сколько самолет будет на этой точке'''
        self.args.append(coordinates)
        self.args.append(flight_number)
        routes = self.routes
        
        input_coordinates = [int(coordinates[:2]),int(coordinates[-2::1])]

        y=0
        distance_by_point_summ=0
        for i in routes[flight_number]['tr']:
            if input_coordinates in routes[flight_number]['tr']:        
                if len(routes[flight_number]['tr'])>y+1:
                    distance_by_point = self.dist(coordinates=routes[flight_number]['tr'], d=y)
                    distance_by_point_summ+=distance_by_point
                    if routes[flight_number]['tr'][y+1]==input_coordinates:
                        distance=distance_by_point
                    y+=1
                else:            
                    break

        #считаем общее время полета
        flight_time=round(distance/int(routes[flight_number]['speed']), 2)

        #конвертируем время взлета в формат datetime
        start_time_convert=self.datetime_converter(routes[flight_number]['start'])
        dist_time_convert=self.dist_time_converter(flight_time)
        arrival_time=start_time_convert + dist_time_convert

        return arrival_time

    def number_of_flights(self, input_time):
        '''Задача №4: Дается время. Нужно определить сколько самолетов находятся в воздухе
        в заданное время'''
        routes = self.routes
        self.args.append(input_time)
        input_time_convert=self.datetime_converter(input_time)

        for i in routes:
            w=0
            distance_summ_by_every_flight=0
            for k in routes[i]['tr']:
                if len(routes[i]['tr'])>w+1:
                    distance=self.dist(coordinates=routes[i]['tr'], d=w)
                    w+=1
                    distance_summ_by_every_flight+=distance
                else:
                    #расчитываем время прилета каждого самолета
                    flight_time_every_plane=round(distance_summ_by_every_flight/int(routes[i]['speed']), 2)
                    start_time_every_plane=self.datetime_converter(routes[i]['start'])
                    dist_time_convert=self.dist_time_converter(flight_time_every_plane)
                    arrival_time_every_plane=start_time_every_plane + dist_time_convert

                    #Проверяем какой из самолетиков находится в воздухе в заданном интервале времени
                    if (input_time_convert>start_time_every_plane
                        and input_time_convert<arrival_time_every_plane):
                        number_of_flights=0
                        number_of_flights+=1
                    break

        return number_of_flights
        
    def dist_to_new_point(self, flight_number, new_coordinates):
        '''Задача №5: На вход дается номер рейса и точка(координаты точки).
        Надо определить какое расстояние от точки начала до заданной'''
        self.args.append(new_coordinates)
        self.args.append(flight_number)

        routes = self.routes

        start_coordinates=[routes[flight_number]['tr'][0][0],routes[flight_number]['tr'][0][1]]
        finish_coordinates=[int(new_coordinates[:2]), int(new_coordinates[3:5])]        
        coordinates=[start_coordinates, finish_coordinates]
        distance = self.dist(coordinates=coordinates, d=0)
       
        return distance




        
